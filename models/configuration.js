var rethinkdb = require('rethinkdb');
var async = require('async');
var dataBase = require('./db');
var db = new dataBase();


class configuration {

  setConfiguration(data, callback){
    async.waterfall([
      function(callback){
        db.connectToDb(function(err, conn){
          if (err){
            return callback(true, "Error al conectar a la base de datos");
          }
          callback(null, conn);
        });
      },
      function(conn, callback){
        rethinkdb.table('configuracion').get(data.id).update({
          "fechaPrecal": data.fechaPrecal,
          "tempPrecal": data.tempPrecal,
          "nombreConfig": data.nombre,
          "t_enc1": data.tenc1,
          "t_enc2": data.tenc2,
          "t_enc3": data.tenc3,
          "temp1": data.temp1,
          "temp2": data.temp2,
          "temp3": data.temp3,
        }).run(conn, function(err, results){
          conn.close();
          if(err){
            return callback(true, "Error al cargar la configuración.");
          }
          callback(null, results);
        });
      }
    ], function(err, data){
      callback(err === null ? false : true, data);
    });
  }

  getConfiguration(conf, callback){
    async.waterfall([
      function(callback) {
        db.connectToDb(function(err,connection) {
          if(err) {
            return callback(true,"Error al conectar a la BD");
          }
          callback( null,connection);
        });
      },
      function(connection,callback) {
        rethinkdb.table('configuracion').get(conf.id).run(connection,function(err,result) {
          if(err) {
            return callback(true,"Error al traer la configuración.");
          }
          // console.log(result);
          callback(null,result);
        });
      }
    ],function(err,data) {
      callback(err === null ? false : true,data);
    });
  }

}

module.exports = configuration;