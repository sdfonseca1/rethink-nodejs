var rethinkdb = require('rethinkdb');
var async = require('async');

class db {

  setupDb() {

    var self = this;
    async.waterfall([
      function(callback) {
        self.connectToRethinkDbServer(function(err,connection) {
          if(err) {
            return callback(true,"Error al conectar a RethinkDB");
          }
          callback(null,connection);
        });
      },
      function(connection,callback) {
        rethinkdb.dbCreate('horno').run(connection,function(err, result) {
          if(err) {
            console.log("Base de datos ya creada.");
          } else {
            console.log("Nueva base de datos creada.");
          }
          callback(null,connection);
        });
      },
      function(connection,callback) {
        rethinkdb.db('horno').tableCreate('configuracion').run(connection,function(err,result) {
          // connection.close();
          if(err) {
            console.log("Tabla CONFIGURACION ya creada.");
          } else {
            console.log("Nueva tabla CONFIGURACION creada.");
          }
          callback(null,connection);
        });
      },
      function(connection,callback) {
        rethinkdb.db('horno').tableCreate('configuraciones').run(connection,function(err,result) {
          // connection.close();
          if(err) {
            console.log("Tabla CONFIGURACIONES ya creada.");
          } else {
            console.log("Nueva tabla CONFIGURACIONES creada.");
          }
          callback(null,connection);
        });
      },
      function(connection,callback) {
        rethinkdb.db('horno').tableCreate('usuarios').run(connection,function(err,result) {
          connection.close();
          if(err) {
            console.log("Tabla USUARIOS ya creada.");
          } else {
            console.log("Nueva tabla USUARIOS creada.");
          }
          callback(null,"Base de datos HORNO correctamente.");
        });
      }
    ],function(err,data) {
      console.log(data);
    });
  }

  connectToRethinkDbServer(callback) {
    rethinkdb.connect({
      host : 'localhost',
      port : 28015
    }, function(err,connection) {
      callback(err,connection);
    });
  }

  connectToDb(callback) {
    rethinkdb.connect({
      host : 'localhost',
      port : 28015,
      db : 'horno'
    }, function(err,connection) {
      callback( err , connection );
    });
  }
}

module.exports = db;