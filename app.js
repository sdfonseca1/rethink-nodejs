var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var http = require('http').Server(app);
var db = require('./models/db');

// Controllers
var dbModel = new db();

// Configuration DB
dbModel.setupDb();
app.use(bodyParser.json());
app.use(require('./controllers'));

http.listen(3100, function(){
  console.log('listening on port 3100');
});